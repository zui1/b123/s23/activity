db.users.insertMany([

		{
			"firstName": "Thor",
			"lastName": "Odinson",
			"email": "iAmTheStrongestAvenger@gmail.com",
			"password": "iAmStrogerThanTheHulk",
			"isAdmin": false
		},
		{
			"firstName": "Tony",
			"lastName": "Stark",
			"email": "IamIRONMAN@gmail.com",
			"password": "coolestBillionaire",
			"isAdmin": false
		},
		{
			"firstName": "Steve",
			"lastName": "Rogers",
			"email": "iCanDoThisAllDay@gmail.com",
			"password": "AVENGERS!",
			"isAdmin": false
		},
		{
			"firstName": "Bruce",
			"lastName": "Banner",
			"email": "Dr.Hulk@gmail.com",
			"password": "hulkSmash",
			"isAdmin": false
		},
		{
			"firstName": "Natasha",
			"lastName": "Romanoff",
			"email": "blackWidow@gmail.com",
			"password": "painOnlyMakesUsStronger",
			"isAdmin": false
		}

	])


db.courses.insertMany([

		{
			"name": "The Python Mega Course",
			"description": "Learn Python from zero to hero",
			"price": 1500,
			"isActive":  true
		},
		{
			"name": "The Basics of HTML & CSS",
			"description": "Beginner's Course to Learn the basics og HTML & CSS",
			"price": 1000,
			"isActive":  true
		},
		{
			"name": "Javascript 101",
			"description": "Learn Javascript for Beginners",
			"price": 1200,
			"isActive":  true
		}


	])


db.users.updateOne({"firstName": "Steve"},{$set:{"isAdmin": true}})

db.users.find({"isAdmin": false})

db.courses.updateOne({"name": "Javascript 101"},{$set:{"isActive": false}})
